import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VideoPalyerComponent } from './video-palyer/video-palyer.component';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';

@NgModule({
  declarations: [VideoPalyerComponent],
  imports: [
    CommonModule,
  ],
  exports: [VideoPalyerComponent]

})
export class PlayerModule { }
