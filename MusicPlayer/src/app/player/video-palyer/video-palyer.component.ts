import { PlaylistService } from './../../playlist.service';
import { VideoDetailComponent } from './../../search/video-detail/video-detail.component';
import { VideoService } from './../../video.service';
import { Component, OnInit, Input, SecurityContext } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-video-palyer',
  templateUrl: './video-palyer.component.html',
  styleUrls: ['./video-palyer.component.scss'],
})
export class VideoPalyerComponent implements OnInit {

  constructor(
    private videoService: VideoService,
    private sanitizer: DomSanitizer,
    private db: PlaylistService
  ) { }

  // videoId: string;
  videoId = 'q8ir8rVl2Z4'; // made by Sylwix
  videoUrl: SafeResourceUrl;
  lista: string[] = [];

 

 ngOnInit() {
    this.makeSafeURL(this.videoId);

    /*this.videoService.currentVideo.subscribe((videoId: string) => {
        this.makeSafeURL(videoId);
        console.log(videoId);
     });

     this.pobierzId().subscribe(ret => {
       this.lista = ret);
       this.makeSafeURL(videoId);
     }*/
    const com = combineLatest(this.videoService.currentVideo, this.pobierzId());
    com.subscribe(([videoId, playlist]) => {
      console.log(videoId, playlist)
      this.lista = playlist;
      this.makeSafeURL(videoId);
      console.log(videoId);
    });

  }

  private makeSafeURL(videoId: string,) {
    const dangerousVideoUrl = 'https://www.youtube.com/embed/'
      //+ videoId + '?color=white'; //działa, jest biały pasek, prawidlowy sposob przekazania
      + videoId + '?version=3&loop=1&playlist=' + this.lista
      + '?color=white'; //tu już nie działa
    //'/ecver=2';
    this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(dangerousVideoUrl);
  }

  pobierzId(): Observable<string[]> {
    return this.db.getPlaylist()
      .pipe(
        map((songList: any) => {
          const newSongList = songList.map(firestoreDoc => {

            return firestoreDoc.payload.doc.data().filmId

          });
          console.log("newlist", newSongList);
          return newSongList;
        })
      );
  }

}
