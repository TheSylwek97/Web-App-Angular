import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoPalyerComponent } from './video-palyer.component';

describe('VideoPalyerComponent', () => {
  let component: VideoPalyerComponent;
  let fixture: ComponentFixture<VideoPalyerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoPalyerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoPalyerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
