import { VideoDetailComponent } from './../video-detail.component';
import { PlaylistService } from './../../../playlist.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Location } from '@angular/common';

@Component({
  selector: 'app-add-to-playlist',
  templateUrl: './add-to-playlist.component.html',
  styleUrls: ['./add-to-playlist.component.scss']
})
export class AddToPlaylistComponent implements OnInit {
 
  constructor(private location: Location,) { }

  ngOnInit() {
  }

  goBack(): void {
    this.location.back();
  }

}
