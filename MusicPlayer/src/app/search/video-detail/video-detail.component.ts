import { AddToPlaylistComponent } from './add-to-playlist/add-to-playlist.component';
import { PlaylistService } from './../../playlist.service';
import { YtApiService } from 'src/app/search/yt-api.service';
import { Component, OnInit, Input } from '@angular/core';
import { VideoService } from 'src/app/video.service';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-video-detail',
  templateUrl: './video-detail.component.html',
  styleUrls: ['./video-detail.component.scss']
})
export class VideoDetailComponent implements OnInit {

  @Input() film: any;
  videoClickedId: string;
  title: SafeHtml;
  newSong: FormGroup;

  constructor(
    private ytApiService: YtApiService,
    public videoService: VideoService,
    private domSanitizer: DomSanitizer,
    private playlistService: PlaylistService,
    private fb: FormBuilder
  ) { }

  onClickImg() {
  }

  onPlayVideoClick(videoId: string): void {
    console.log('playvideoclick', videoId);
    this.videoService.currentVideo.next(videoId);
  }

  onAddToFileListClick(videoId: string, title: SafeHtml, author: string) {
    console.log(this.newSong.value);
    console.log(videoId, title);
    const song: SongInPlaylist = {
      // ...this.newSong.value
      filmId : videoId,
      title: this.film.snippet.title,
      channelTitle: author,
    };
    this.playlistService.addSong(song);
    // this.playlistService.addSong(song);
  }

  ngOnInit() {
    this.title = this.domSanitizer.bypassSecurityTrustHtml(this.film.snippet.title);

    this.newSong = this.fb.group({
      title: [''],
      videoId: [''],
      author: ['']
    });
  }


}
