import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchComponent } from './search/search.component';
import { VideoDetailComponent } from './video-detail/video-detail.component';
import {MatButtonModule, MatInputModule, MatCardModule, MatFormFieldModule} from '@angular/material';
import { AddToPlaylistComponent } from './video-detail/add-to-playlist/add-to-playlist.component';

@NgModule({
  declarations: [SearchComponent, VideoDetailComponent, AddToPlaylistComponent],
  imports: [
    CommonModule,
    MatButtonModule, MatInputModule, MatCardModule, MatFormFieldModule
  ],
  exports: [SearchComponent, VideoDetailComponent]
})
export class SearchModule { }
