import { VideoService } from './../../video.service';
import { YtApiService } from './../yt-api.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  songsList: any[] = [];
  songsList$ = new Observable();

  // ingUrl: string;

  constructor(private ytApiService: YtApiService) { }

  ngOnInit() {
  }

  searchSong(event: any) {
    const searchTxt = event.target.value;
    if (searchTxt !== '') {
    this.ytApiService.searchSong(searchTxt)
     .pipe(
       map(
         ytSerachObj => {
          //  console.log(ytSerachObj)
           return ytSerachObj.items;
         }
       ),
       map(
         items => {
           return items.map(item => {
            return {
            videoId: item.id.videoId,
            snippet: item.snippet
             };
           }
       );
         }
       )
     )
     .subscribe(ret => {
      this.songsList = ret;
      console.log(ret);

      });
    } else {
      this.songsList = [];
    }
   }
}
