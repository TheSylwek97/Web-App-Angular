import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class YtApiService {
  ytApiUrl = 'https://www.googleapis.com/youtube/v3/search';
  ytApiKey = environment.ytApiKey;

  constructor(private http: HttpClient) { }

  searchSong(searchTxt: string): Observable<any> {
    const url = `${this.ytApiUrl}?part=snippet&q=${searchTxt}&key=${environment.ytApiKey}&type=video`;
    //const url = `${this.ytApiUrl}search/movie?api_key=${this.ytApiKey}&query=${searchTxt}`
    return this.http.get(url);
  }
}
