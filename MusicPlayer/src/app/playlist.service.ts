import { VideoService } from './video.service';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlaylistService {

  private firestoreList: SongInPlaylist[] = [];
  currentVideo = new Subject();
  constructor(private db: AngularFirestore,
              public videoService: VideoService ) { }



    /*addSong(song: SongInPlaylist) {
      this. firestoreList.push(song);
      localStorage.setItem(
        ' firestoreList',
        JSON.stringify(this. firestoreList)
      );
    }*/

    addSong(song: SongInPlaylist) {
      console.log(song);
      return this.db.collection('filmy').add(song);

    }

    getPlaylist(): Observable<any> {
      return this.db.collection('filmy').snapshotChanges();
    }

    getSong(id: string) {
      return this.db.doc('filmy/' + id).get();
    }

    deleteSong(id: string) {
      return this.db.doc('filmy/' + id).delete();
    }

}
