import { VideoService } from 'src/app/video.service';
import { PlaylistService } from './../playlist.service';
import { Component, OnInit } from '@angular/core';
import { Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { Location } from '@angular/common';


@Component({
  selector: 'app-play-list',
  templateUrl: './play-list.component.html',
  styleUrls: ['./play-list.component.scss']
})
export class PlayListComponent implements OnInit {

  constructor(private db: PlaylistService,
              private location: Location,
              private videoService: VideoService) { }

  songList: Observable<any>;
   public playSongInPlaylist :string[] = [];

  ngOnInit() {
    this.songList = this.db.getPlaylist()
      .pipe(
        map( songList => {
          const newSongList = songList.map( firestoreDoc => {
            return {
              ...firestoreDoc.payload.doc.data(),
              id: firestoreDoc.payload.doc.id
            };
          });
          return newSongList;
        })
      );

      

      


  }

  onBtnUsunClick(id: string) {
    this.db.deleteSong(id);
  }

  goBack(): void {
    this.location.back();
  }

  ponPlayVideoClick(videoId: string): void {
    //console.log('playvideoclick', videoId)
    this.videoService.currentVideo.next(videoId);
    
  }

}
