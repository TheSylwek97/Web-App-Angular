
import { AddToPlaylistComponent } from './search/video-detail/add-to-playlist/add-to-playlist.component';
import { PlayListComponent } from './play-list/play-list.component';
import { VideoPalyerComponent } from './player/video-palyer/video-palyer.component';
import { SearchComponent } from 'src/app/search/search/search.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'search',
    component: SearchComponent
  },
  {
    path: 'player',
    component: VideoPalyerComponent
  },
  {
    path: '',
    pathMatch: 'full',
    component: SearchComponent
  },
  {
    path: 'playlist',
    component: PlayListComponent
  },
  {
    path: 'addtoplaylist',
    component: AddToPlaylistComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
