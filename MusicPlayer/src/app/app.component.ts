import { AppModule } from './app.module';
import { YtApiService } from 'src/app/search/yt-api.service';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  ytApi: any;

  constructor( private ytApiService: YtApiService) {}
  ngOnInit(): void {

  }
  title = 'MusicPlayer';


}

