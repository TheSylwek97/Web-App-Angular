import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VideoService {

  private firestoreList: SongInPlaylist [] = [];
  currentVideo = new Subject();

  constructor() { }

  getSongsFromLocalStorage() {
    this.firestoreList = JSON.parse(localStorage.getItem('firestoreList'));
  }

  addSong(song: SongInPlaylist) {
    this.firestoreList.push(song);
    localStorage.setItem(
      'firestoreList',
      JSON.stringify(this.firestoreList)
    );
  }

  getSong(id: string): SongInPlaylist{
    return this.firestoreList.find(el => el.filmId === id);
  }

  getSongs(): SongInPlaylist[] {
    return this.firestoreList;
  }
}
